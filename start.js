var discord = require("discord.js");
var express = require('express');
var mongoose = require('mongoose');

var db = require('./model/db');
var user = require('./model/user');

const app = express();
const shifu = new discord.Client();
const env = process.env;

app.get('/', function (req, res) {
  res.send('Shifu Bot');
});

shifu.on("message", function(message) {
    var msg = message.content.trim();
    if(msg === "!ping") {
        shifu.sendMessage(message, "Start Team Pratice");
    }

    if(msg === "!guia") {
        shifu.reply(message, "http://en.wulin.webzen.com/GameGuide");
    }

    if(msg.startsWith("!register")) {
        var content_split = msg.splice(9).trim().split(' ', 2);

        nick = content_split[0];
        guild = content_split[1];

        User = mongoose.model('User')
        User.find({uid: message.author.id}, function(e, results) {
            if(results.length >= 1) {
                User.update(results[0], {'$set': {
                    character: {
                        name: nick,
                        guild: guild
                    }
                }}, function(err) {
                    console.log(err);
                });
            } else {
                User.create({
                    uid : message.author.id,
                    character: {
                        name: nick,
                        guild: guild
                    }
                });
            };
        });
    }
});

shifu.on("ready", function(message) {
    var interval = setInterval( function() {
        var now = new Date;
        if(now.getSeconds() == 0) {
            setInterval(server_announcement, 60000);
            clearInterval(interval);
        }
        console.log('checkSeconds: ' + now.getSeconds());
    }, 1000);
});

function server_announcement() {
    var canal_geral = shifu.channels.get('id', 152170511206580224); // Canal Geral do LotusBranco e Aliados
    var now = new Date;

    if((now.getHours() == 8 || now.getHours() == 20 || now.getHours() == 14) && now.getMinutes() == 25) {
        canal_geral.sendMessage('@everyone Impart Knowledge start in 5m');
    }

    if((now.getHours() == 8 || now.getHours() == 20 || now.getHours() == 14) && now.getMinutes() == 30) {
        canal_geral.sendMessage('@everyone Impart Knowledge started');
    }

    if(now.getDay() != 0) {
        if(now.getHours() == 19  && now.getMinutes() == 50) {
            canal_geral.sendMessage('@everyone Steel Books start in 5m');
        }

        if(now.getHours() == 19  && now.getMinutes() == 55) {
            canal_geral.sendMessage('@everyone Steel Books started');
        }
    }

    if(now.getHours() == 21  && now.getMinutes() == 00) {
        if(now.getDay() == 1) {
            canal_geral.sendMessage('@everyone Kidnap in Jinling Started');
        }
        if(now.getDay() == 2) {
            canal_geral.sendMessage('@everyone Kidnap in Chengdu Started');
        }
        if(now.getDay() == 3) {
            canal_geral.sendMessage('@everyone Kidnap in Yanjing Started');
        }
        if(now.getDay() == 4) {
            canal_geral.sendMessage('@everyone Kidnap in Luoyang Started');
        }
        if(now.getDay() == 5) {
            canal_geral.sendMessage('@everyone Kidnap in Suzhou Started');
        }
    }
}

function connect() {
    app.listen(env.NODE_PORT || 3000, function () {
        console.log('Example app listening on port 3000!');
        shifu.loginWithToken("TOKEN_DISCORD_BOT", function(error, token) {
            console.log("Bot Iniciado");
        })
    });
}

connect();
