var mongoose = require('mongoose');
var userSchema = new mongoose.Schema({
  uid: String,
  character: { name: String, guild: String }
});
mongoose.model('User', userSchema);
